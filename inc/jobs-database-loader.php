<?php
/*
 *  rob_page_jobs_feed is used to jobs feed
 */
function rob_db_loader(){
    global $wpdb;
        $feed_url   = 'https://localeyesite.com/feed';
        $display    = '';
        $dateformat = 'm.d.y';
    $xml = simplexml_load_file($feed_url, 'SimpleXMLElement',LIBXML_NOCDATA);
    if ($xml) {
        $wpdb->query("DELETE FROM wp_rob_jobs");
        $p_cnt = count($xml->job); 
        $arrxml = $xml->job;
        for($i = 0; $i < $p_cnt; $i++) {
            $url    = $arrxml[$i]->url;
            $title  = $arrxml[$i]->title;
            $date   = date('m.d.y', strtotime($arrxml[$i]->date));              
            $company= $arrxml[$i]->company;
            $city   = $arrxml[$i]->city;
            $state  = $arrxml[$i]->state;
            $postalcode = $arrxml[$i]->postalcode;
            $country    = $arrxml[$i]->country;
            $jobtype    = $arrxml[$i]->jobtype;         

            $wpdb->insert(
                'wp_rob_jobs',
                array(
                        'company' => $company,
                        'job_title' => $title,
                        'job_url' => $url,
                        'city' => $city,
                        'state' => $state,
                        'job_type' => $jobtype,
                        'post_date' => $date
                    ),
                array(
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s'
                    )
                );
            /*
            $wpdb->query(
                $wpdb->prepare(
                    "
                        INSERT INTO $wpdb->rob_jobs
                        ( company, job_title, job_url, city, state, job_type, post_date )
                        VALUES ( %s, %s, %s, %s, %s, %s, %s )
                    ",
                    $company,
                    $title,
                    $url,
                    $city,
                    $state,
                    $jobtype,
                    $date
                    ) );
                    */
        }           
    } else {
        $arrdata = 'Could not load intro XML file. '.date('Y-m-d H:i:s');
        rob_db_setting_insert('Feed Import Error',$arrdata);
    }   
    echo $arrdata;
}
function rob_db_setting_insert($option_name,$option_value){
    global $wpdb;
    $wpdb->insert('wp_rob_jobs_settings',array('option_name'=>$option_name,'option_value'=> $option_value));
}
?>