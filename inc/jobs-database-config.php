<?php
	function jobs_sample_db_data($wpdb){
		$table_name = $wpdb->prefix . 'fdi_settings';
		$option = 'sample option';
		$value = 'sample value';
		$wpdb->insert($table_name,array('option_name'=>$option,'option_value'=> $value));
	}
	function jobs_db_install () {
	   global $wpdb;
	   $charset_collate = $wpdb->get_charset_collate();
	   require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	   $table_name = $wpdb->prefix . "rob_jobs_settings"; 
	   $charset_collate = $wpdb->get_charset_collate();
        $sql = "CREATE TABLE " . $table_name . " (
            id INT NOT NULL AUTO_INCREMENT, 
            option_name TEXT NOT NULL, 
            option_value TEXT NOT NULL,
            PRIMARY KEY  (id)
        ) ". $charset_collate .";";
        dbDelta($sql);

        $table_name = $wpdb->prefix . "rob_jobs";
        $sql2 = "CREATE TABLE " . $table_name . " (
        	id INT NOT NULL AUTO_INCREMENT,
        	company TEXT,
        	job_title TEXT,
        	job_url TEXT,
            city TEXT,
            state TEXT,
            job_type TEXT,
            post_date TEXT,
        	PRIMARY KEY  (id)
        ) " . $charset_collate .";";
        dbDelta($sql2);
	}
?>