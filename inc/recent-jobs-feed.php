<?php
/**
 * Recent jobs rss feed widget
 */
 
class Recent_Jobs_Feed_Widgets extends WP_Widget {
	
	public function __construct()
    {
        $widget_details = array(
            'classname' => 'widget_recent_entries',
            'description' => __( "Recent jobs Rss feed from API Call.", 'rob-feed-import' )
        );
 
        parent::__construct( 'feed_by_recent_jobs', 'Feed by Recent jobs Widget', $widget_details );
    }
	
	
	// widget form creation
	public function form( $instance ) {
		$title 	= isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$text 	= isset( $instance['text'] ) ? esc_attr( $instance['text'] ) : '';
		$dateformat	= array('F j, Y, g:i a'=>'March 10, 2001, 5:16 pm', 'm.d.y'=>'03.10.01', 'j, n, Y'=> '10, 3, 2001', 'Y-m-d H:i:s'=> '2001-03-10 17:16:18', 'F j, Y'=>'March 10, 2001');
		$html_dateformat = '<select id="'.$this->get_field_id('dateformat').'" class="widefat" name="'.$this->get_field_name('dateformat').'">';
		$html_dateformat .= '<option value="">Select one...</option>';
		foreach ($dateformat as $dkey=>$dval) {			
			$selected = ($instance['dateformat'] == $dkey) ? ' selected="selected"' : '';
			$html_dateformat .= '<option value="' . $dkey .'"' . $selected . '>' . $dval . '</option>';
		}
		$html_dateformat .= '</select>';
		
		//echo date('F j, Y', strtotime('Mon, 19 Dec 2016 18:31:10 EST'));
		
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'rob-feed-import'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Feed URL:', 'rob-feed-import'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" type="text" value="<?php echo $text; ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'dateformat' ); ?>"><?php _e( 'DateFormat:', 'rob-feed-import' ); ?></label>
			<?php echo $html_dateformat;?>
		</p>	
		<?php
	}	
	
	// update widget
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['text'] = strip_tags( $new_instance['text'] );
		$instance['dateformat'] = strip_tags( $new_instance['dateformat'] );
		return $instance;
	}
	
	// display widget
	function widget($args, $instance) {
		global $wpdb;
	    extract( $args );
		$head_title = apply_filters('widget_title', $instance['title']);
		$dateformt = (apply_filters('widget_dateformat', $instance['dateformat']) !='') ? apply_filters('widget_dateformat', $instance['dateformat']) : 'm.d.y';
		$feed_url = (apply_filters('widget_text', $instance['text']) !='') ? apply_filters('widget_text', $instance['text']) : 'https://localeyesite.com/feed';

		$rob_job_feeds = $wpdb->get_results("SELECT company, job_title, job_url, city, state, job_type, post_date FROM wp_rob_jobs LIMIT 5");
		
			$arrdata	= "<ul>";
			foreach ( $rob_job_feeds as $rob_job_feed ){
				$company = $rob_job_feed->company;
				$joburl = $rob_job_feed->job_url;
				$title = "<a href='".$joburl."' target='_blank'>".$rob_job_feed->job_title."</a>";
				$city = $rob_job_feed->city;
				$state = $rob_job_feed->state;
				$jobtype = $rob_job_feed->job_type;
				$date = $rob_job_feed->post_date;
				$address	= $city.', '.$state;				
				$arrdata	.= "<li><p>{$date}</p><p>{$title}</p><p>{$address}</p></li>";
			}			
			$arrdata	.= "</ul>";

		echo $before_widget;
		echo '<div class="widget-text wp_widget_plugin_box">';
	   // Check if title is set
	   
	   echo $before_title . $head_title . $after_title;
	   echo $arrdata;
	   echo '</div>';
	   echo $after_widget;
	}
	
}