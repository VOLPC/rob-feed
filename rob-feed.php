<?php
/**
 * Plugin Name: ReviewOB Custom Feed Import
 * Description: This plugin that displays 3rd party Rss Feeds in page and widgets..
 * Version: 1.2
 * Text Domain: rob-feed
 * Author: Jobson Interactive
 * Author URI: https://www.jobsoninteractive.com/
 */
 
// Setup the Database
include_once dirname( __FILE__ ) . '/inc/jobs-database-config.php';
include_once dirname( __FILE__ ) . '/inc/jobs-database-loader.php';

define('ROB_PLUGIN_FOLDER',  plugins_url(NULL, __FILE__));

define('ROB_SUBDIR_ASSETS',  ROB_PLUGIN_FOLDER . '/assets/'); 

register_activation_hook( __FILE__, 'jobs_db_install');
register_activation_hook( __FILE__, 'rob_db_loader');
register_activation_hook(__FILE__, "robfeed_install");
register_deactivation_hook(__FILE__, 'robfeed_uninstall');

function robfeed_install()
{
	require_once(ABSPATH.'wp-admin/includes/upgrade.php');
	add_option("robfeed_db_version", '1.1');
	if (! wp_next_scheduled ( 'jobfeed_hourly_event' )) {
	wp_schedule_event(time(), 'hourly', 'jobfeed_hourly_event');
    }
}

function robfeed_uninstall()
{
	delete_option('robfeed_db_version');
	wp_clear_scheduled_hook('jobfeed_hourly_event');
}
 

function my_script_enqueuer() {
	wp_register_script( "robfeed_script", WP_PLUGIN_URL.'/rob-feed-import/assets/sorttable.js', array('jquery') );	
	wp_register_script( "probfeed_script", WP_PLUGIN_URL.'/rob-feed-import/assets/jquery.simplePagination.js', array('jquery') );	
	wp_register_script( "robfeed_page_script", WP_PLUGIN_URL.'/rob-feed-import/assets/pagination.js', array('jquery') );	
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'probfeed_script' );
	wp_enqueue_script( 'robfeed_script' );
	wp_enqueue_script( 'robfeed_page_script' );
}	
add_action( 'init', 'my_script_enqueuer' );
 
 
class Custom_Widgets {
	public function __construct() {
		add_action( 'widgets_init', array( $this, 'load' ), 9 );
		add_action( 'widgets_init', array( $this, 'init' ), 10 );
		register_uninstall_hook( __FILE__, array( __CLASS__, 'uninstall' ) );
	}
	
	public function load() {
		$dir = plugin_dir_path( __FILE__ );
		include_once( $dir . 'inc/recent-jobs-feed.php' );
	}
	
	public function init() {
		if ( ! is_blog_installed() ) {
			return;
		}
		register_widget( 'Recent_Jobs_Feed_Widgets' );
	}

	public function uninstall() {}
		
}	
 
$custom_widgets = new Custom_Widgets();



/*
 *  rob_page_jobs_feed is used to jobs feed
 *  @param: str Feed url
 *  @param: int limits
 *  @param: str dateformat
 */
add_shortcode('ROBJOBSFEED', 'ROB_jobs_feed_list' );

function ROB_jobs_feed_list($atts){
	global $wpdb;
	$feed_url 	= 'https://localeyesite.com/feed';
	$display 	= '';
	$dateformat	= 'm.d.y';
	$rob_job_feeds = $wpdb->get_results(
		"SELECT company, job_title, job_url, city, state, job_type, post_date FROM wp_rob_jobs"
	);

	$arrdata	= '<table id="rob_jobs_table" class="table table-striped sortable"><thead><tr><th>Company</th><th>Job Title</th><th>City</th><th>State</th><th>Job Type</th><th>Date</th></tr></thead><tbody>';
	foreach ( $rob_job_feeds as $rob_job_feed ){
		$company = $rob_job_feed->company;
		$joburl = $rob_job_feed->job_url;
		$title = "<a href='".$joburl."' target='_blank'>".$rob_job_feed->job_title."</a>";
		$city = $rob_job_feed->city;
		$state = $rob_job_feed->state;
		$jobtype = $rob_job_feed->job_type;
		$date = $rob_job_feed->post_date;

		$arrdata	.= "<tr><td>".$company."</td><td>".$title."</td><td>".$city."</td><td>".$state."</td><td>".$jobtype."</td><td>".$date."</td></tr>";
	}
	$arrdata	.= "</tbody></table>";

	return $arrdata;
}

add_action('jobfeed_hourly_event', 'rob_jobs_hourly_update');
function rob_jobs_hourly_update(){
	//update the feed every hour
	rob_db_loader();
}